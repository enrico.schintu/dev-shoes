import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginAreaComponent} from './core/components/login-area/login-area.component';
import {DashboardComponent} from './core/components/dashboard/dashboard.component';
import {DashboardGuard} from './core/guards/dashboard.guard';


const routes: Routes = [
  {path: 'login', component: LoginAreaComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [DashboardGuard]},
  {path: '**', redirectTo: 'dashboard'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
