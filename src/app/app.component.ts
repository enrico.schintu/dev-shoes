import {Component, OnInit} from '@angular/core';
import {TimeHandlerService} from './core/services/time-handler.service';
import {PlayerConfigurationService} from './core/services/player-configuration.service';
import {TriggerAction} from './core/models/interfaces';
import {TimeTriggerType} from './core/models/enums';
import {NotificationsService} from './shared/notifications/services/notifications.service';
import {MessageType} from './shared/models/enums';
import {MarketplaceService} from './core/services/marketplace.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private timeHandlerService: TimeHandlerService,
              private playerConfigurationService: PlayerConfigurationService,
              private notificationsService: NotificationsService) {
  }

  ngOnInit(): void {
    this.playerConfigurationService.startGame();

    const happyNewYearMsg: TriggerAction = {
      triggerType: TimeTriggerType.YEARLY_TRIGGER,
      action: (y) => this.notificationsService.showNotifications({message: `🎉 Happy new #${y} Year! 🎉`, level: MessageType.SUCCESS})
    };

    this.timeHandlerService.addTriggerActions(happyNewYearMsg);
  }
}
