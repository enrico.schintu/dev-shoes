export enum MessageType {
  INFO = 'Info',
  SUCCESS = 'Success',
  WARNING = 'Warning',
  DANGER = 'Danger'
}
