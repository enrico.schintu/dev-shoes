import {MessageType} from './enums';

export interface EsMessage {
  message: string;
  title?: string;
  level?: MessageType;
}
