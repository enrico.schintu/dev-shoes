import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EsNotification} from '../../models/notification';
import {MessageType} from '../../../models/enums';

@Component({
  selector: 'app-notification',
  template: `
      <div class="notify-wrapper {{messageTypeStyle}}">
          <div *ngIf="!!notification.title" class="notify-header">
              <span class="notify-title">{{notification.title}}</span>
          </div>
          <div class="notify-body">
              <span class="notify-message">{{notification.message}}</span>
              <span class="notify-close" (click)="closeNotification($event)">❌</span>
          </div>
      </div>
  `,
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() notification: EsNotification = null;

  @Output() handleClose: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  closeNotification($event: MouseEvent) {
    this.handleClose.emit(this.notification.id);
  }

  get messageTypeStyle(): string {
    switch (this.notification.level) {
      case MessageType.SUCCESS:
        return 'n-success';
      case MessageType.WARNING:
        return 'n-warning';
      case MessageType.DANGER:
        return 'n-danger';
      case MessageType.INFO:
      default:
        return 'n-info';
    }
  }
}
