import {Component, OnInit} from '@angular/core';
import {NotificationsService} from '../../services/notifications.service';
import {EsNotification} from '../../models/notification';

@Component({
  selector: 'app-notification-wrapper',
  template: `
      <div class="notifications-wrapper">
          <ng-container *ngFor="let notification of notifications">
              <app-notification [notification]="notification" (handleClose)="hideNotification($event)"></app-notification>
          </ng-container>
      </div>
  `,
  styleUrls: ['./notification-wrapper.component.scss']
})
export class NotificationWrapperComponent implements OnInit {

  constructor(private notificationsService: NotificationsService) {
  }

  ngOnInit() {
  }

  public hideNotification(id: number): void {
    this.notificationsService.hideNotifications(id);
  }

  get notifications(): EsNotification[] {
    return this.notificationsService.notifications;
  }

}
