import {MessageType} from '../../models/enums';
import {EsMessage} from '../../models/interfaces';

export class EsNotification implements EsMessage {
  id: number;
  level: MessageType;
  message: string;
  title?: string;

  constructor(id: number, esMessage: EsMessage) {
    this.id = id;
    this.level = esMessage.level;
    this.message = esMessage.message;
    this.title = esMessage.title;
  }
}
