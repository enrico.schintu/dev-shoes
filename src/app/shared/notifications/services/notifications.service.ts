import {Injectable} from '@angular/core';
import {NOTIFY_DEFAULT_TIMEOUT} from '../config/notifications';
import {EsNotification} from '../models/notification';
import {EsMessage} from '../../models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  public notifications: EsNotification[] = [];

  private sequence: number = 0;

  constructor() {
  }

  public showNotifications(...messages: EsMessage[]): void {
    messages.forEach(msg => {
      const notification = new EsNotification(this.sequence, msg);
      setTimeout(() => this.hideNotifications(notification.id), NOTIFY_DEFAULT_TIMEOUT);
      this.notifications.push(notification);
      this.sequence++;
    });
  }

  public hideNotifications(...ids: number[]): void {
    ids.forEach(id => this.notifications = this.notifications.filter(active => active.id !== id));
  }
}
