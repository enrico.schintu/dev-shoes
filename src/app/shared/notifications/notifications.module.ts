import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotificationsService} from './services/notifications.service';
import {NotificationWrapperComponent} from './components/notification-wrapper/notification-wrapper.component';
import {SharedModule} from '../shared.module';
import {NotificationComponent} from './components/notify/notification.component';


@NgModule({
  declarations: [NotificationWrapperComponent, NotificationComponent],
  imports: [
    CommonModule,
    SharedModule,
  ],
  providers: [
    NotificationsService
  ],
  exports: [
    NotificationWrapperComponent
  ]
})
export class NotificationsModule {
}
