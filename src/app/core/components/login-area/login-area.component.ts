import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {StarterInfo} from '../../models/interfaces';
import {PlayerConfigurationService} from '../../services/player-configuration.service';
import {WorkerGender} from '../../models/enums';

@Component({
  selector: 'app-login-area',
  templateUrl: './login-area.component.html',
  styleUrls: ['./login-area.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginAreaComponent implements OnInit {

  public title = 'IN DEVELOPER SHOES';
  public model: StarterInfo = {username: null, companyName: null, gender: WorkerGender.MALE};

  constructor(private playerConfigurationService: PlayerConfigurationService) {
  }

  ngOnInit() {
  }

  public startGame(event: Event): void {
    if (this.model.username && this.model.companyName) {
      this.playerConfigurationService.resetGame(this.model);
    }
  }

}
