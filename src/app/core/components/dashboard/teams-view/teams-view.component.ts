import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Team} from '../../../models/team';
import {Project} from '../../../models/project';
import {ProjectAssignation} from '../../../models/interfaces';

@Component({
  selector: 'app-teams-view',
  template: `
      <div id="workersContainer" class="p-3">
          <div *ngFor="let team of teams">
              <span><b>{{team.name}}</b></span>
              <ul class="list-group">
                  <app-workers-view [workers]="team.workers"
                                    [availableProjects]="availableProjects"
                                    (dismissWorker)="handleDismissWorker($event)"
                                    (assignWorkerToProject)="handleAssignWorkerToProject($event)">
                  </app-workers-view>
              </ul>
          </div>
      </div>
  `,
  styleUrls: ['./teams-view.component.scss']
})
export class TeamsViewComponent implements OnInit {

  @Input() teams: Team[] = [];
  @Input() availableProjects: Project[] = [];
  @Output() dismissWorker: EventEmitter<number> = new EventEmitter<number>();
  @Output() assignWorkerToProject: EventEmitter<ProjectAssignation> = new EventEmitter<ProjectAssignation>();

  constructor() {
  }

  ngOnInit() {
  }

  handleDismissWorker(id: number) {
    this.dismissWorker.emit(id);
  }

  public handleAssignWorkerToProject(projectAssignation: ProjectAssignation): void {
    this.assignWorkerToProject.emit(projectAssignation);
  }
}
