import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HeaderInfo} from '../../../models/interfaces';

@Component({
  selector: 'app-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardHeaderComponent implements OnInit {

  @Input() headerInfo: HeaderInfo = null;
  @Output() marketPlaceToggle: EventEmitter<Event> = new EventEmitter<Event>();


  constructor() {
  }

  ngOnInit() {
  }

  public toggleMarketPlace(event: Event): void {
    this.marketPlaceToggle.emit(event);
  }

}
