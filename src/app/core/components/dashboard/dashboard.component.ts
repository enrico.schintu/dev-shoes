import {Component, OnInit} from '@angular/core';
import {Worker} from '../../models/worker';
import {PlayerConfigurationService} from '../../services/player-configuration.service';
import {Team} from '../../models/team';
import {TimeHandlerService} from '../../services/time-handler.service';
import {HeaderInfo, ProjectAssignation} from '../../models/interfaces';
import {Project} from '../../models/project';
import {MarketplaceService} from '../../services/marketplace.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public marketPlaceVisible: boolean = false;

  constructor(public playerConfigurationService: PlayerConfigurationService,
              public timeHandlerService: TimeHandlerService,
              private marketplaceService: MarketplaceService) {
  }

  ngOnInit() {
  }

  public toggleMarketPlace(event: Event): void {
    this.marketPlaceVisible = !this.marketPlaceVisible;
  }

  public acquireProject(id: number): void {
    this.marketplaceService.acquireProject(id);
  }

  public acquireWorker(id: number) {
    this.marketplaceService.acquireWorker(id);
  }

  public handleDismissWorker(id: number): void {
    this.playerConfigurationService.dismissWorker(id);
  }

  public handleAssignWorkerToProject(projectAssignation: ProjectAssignation): void {
    this.playerConfigurationService.assignWorkerToProject(projectAssignation);
  }

  public handleGameSpeedChange(value: number): void {
    this.timeHandlerService.changeGameSpeed(value);
  }

  public handleSaveGame(event: Event): void {
    this.playerConfigurationService.saveGame();
  }

  public handleLoadGame(event: Event): void {
    this.playerConfigurationService.loadSaveGame();
  }

  public handleResetGame(event: Event): void {
    this.playerConfigurationService.initGame();
  }

  get workers(): Worker[] {
    return this.playerConfigurationService.company.workers;
  }

  get teams(): Team[] {
    return this.playerConfigurationService.company.teams;
  }

  get projects(): Project[] {
    return this.playerConfigurationService.activeProjects;
  }

  get headerInfo(): HeaderInfo {
    return {currentMoney: this.playerConfigurationService.companyMoney, currentTime: this.timeHandlerService.currentTime};
  }

  get availableProjects(): Project[] {
    return this.marketplaceService.availableProjects;
  }

  get availableWorkers(): Worker[] {
    return this.marketplaceService.availableWorkers;
  }

  get freeWorkers(): Worker[] {
    return this.workers.filter(w => !w.currentProjectId);
  }
}
