import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-savegame-panel',
  templateUrl: './savegame-panel.component.html',
  styleUrls: ['./savegame-panel.component.scss']
})
export class SavegamePanelComponent implements OnInit {

  @Output() saveGameRequest: EventEmitter<Event> = new EventEmitter<Event>();
  @Output() loadGameRequest: EventEmitter<Event> = new EventEmitter<Event>();
  @Output() resetGameRequest: EventEmitter<Event> = new EventEmitter<Event>();

  constructor() {
  }

  ngOnInit() {
  }

  saveGame(event: MouseEvent): void {
    event.stopPropagation();
    this.saveGameRequest.emit(event);
  }

  loadGame(event: MouseEvent): void {
    event.stopPropagation();
    this.loadGameRequest.emit(event);
  }

  resetGame(event: MouseEvent): void {
    event.stopPropagation();
    this.resetGameRequest.emit(event);

  }
}
