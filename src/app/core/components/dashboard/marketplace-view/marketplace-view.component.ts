import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Project} from '../../../models/project';
import {Worker} from '../../../models/worker';

@Component({
  selector: 'app-marketplace-view',
  templateUrl: './marketplace-view.component.html',
  styleUrls: ['./marketplace-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarketplaceViewComponent implements OnInit {

  @Input() availableProjects: Project[] = [];
  @Input() availableWorkers: Worker[] = [];

  @Output() projectAcquired: EventEmitter<number> = new EventEmitter<number>();
  @Output() workerAcquired: EventEmitter<number> = new EventEmitter<number>();
  @Output() toggleMenu: EventEmitter<Event> = new EventEmitter<Event>();

  constructor() {
  }

  ngOnInit() {
  }

  public handleAddProject(id: number): void {
    this.projectAcquired.emit(id);
    this.handleToggleMarketMenu();
  }

  public handleAddWorker(id: number): void {
    this.workerAcquired.emit(id);
    this.handleToggleMarketMenu();
  }

  public handleToggleMarketMenu(event?: Event): void {
    this.toggleMenu.emit(event);
  }
}
