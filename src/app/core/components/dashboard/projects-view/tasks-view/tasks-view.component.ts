import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Task} from '../../../../models/task';

@Component({
  selector: 'app-tasks-view',
  templateUrl: './tasks-view.component.html',
  styleUrls: ['./tasks-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksViewComponent implements OnInit {

  @Input() tasks: Task[] = [];
  @Input() showDetails: boolean = false;

  constructor() {
  }

  ngOnInit() {
  }

}
