import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Project, ProjectDOM} from '../../../models/project';
import {Worker} from '../../../models/worker';
import {ProjectAssignation} from '../../../models/interfaces';

@Component({
  selector: 'app-projects-view',
  templateUrl: './projects-view.component.html',
  styleUrls: ['./projects-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectsViewComponent implements OnInit {

  @Input() projects: Project[] = [];
  @Input() availableDevelopers: Worker[] = [];
  @Output() assignWorkerToProject: EventEmitter<ProjectAssignation> = new EventEmitter<ProjectAssignation>();

  constructor(private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  public toggleProjectDetails(project: ProjectDOM) {
    project.showDetails = !project.showDetails;
    this.cdr.markForCheck();
  }

  /**
   * Show the free developers menu. Also close other project developers menu if open.
   */
  public toggleDevelopersMenu(event: Event, project: ProjectDOM) {
    event.preventDefault();
    event.stopPropagation();
    this.currentProjects.filter(f => f.id !== project.id && f.showDevelopers).forEach(p => p.showDevelopers = false);
    project.showDevelopers = !project.showDevelopers;
    this.cdr.markForCheck();
  }

  closeDevMenu(project: ProjectDOM) {
    project.showDevelopers = false;
  }

  handleAssignDeveloper(worker: Worker, project: Project) {
    this.assignWorkerToProject.emit({projectId: project.id, workerId: worker.id});
  }

  get currentProjects(): ProjectDOM[] {
    return this.projects.map(p => p as ProjectDOM);
  }
}
