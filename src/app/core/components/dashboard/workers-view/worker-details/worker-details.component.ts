import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Worker} from '../../../../models/worker';
import {Project} from '../../../../models/project';
import {ProjectAssignation} from '../../../../models/interfaces';

@Component({
  selector: 'app-worker-details',
  templateUrl: './worker-details.component.html',
  styleUrls: ['./worker-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkerDetailsComponent implements OnInit {
  @Input() worker: Worker = null;
  @Input() availableProjects: Project[] = [];
  @Output() dismissWorker: EventEmitter<number> = new EventEmitter<number>();
  @Output() assignWorkerToProject: EventEmitter<ProjectAssignation> = new EventEmitter<ProjectAssignation>();

  public showWorkerMenu: boolean = false;

  constructor() {
  }

  ngOnInit() {
  }

  public toggleWorkerMenu(event: Event): void {
    event.stopPropagation();
    this.showWorkerMenu = !this.showWorkerMenu;
  }

  public handleDismissWorker(worker: Worker) {
    this.dismissWorker.emit(worker.id);
  }

  sendToHolidays(event: MouseEvent, worker: Worker): void {
    event.stopPropagation();
    event.preventDefault();
    worker.sendInHolidays();
  }

  assignToProject(worker: Worker, project: Project) {
    this.assignWorkerToProject.emit({projectId: project.id, workerId: worker.id});
  }
}
