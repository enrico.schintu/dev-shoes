import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Worker} from '../../../models/worker';
import {Project} from '../../../models/project';
import {ProjectAssignation} from '../../../models/interfaces';

@Component({
  selector: 'app-workers-view',
  template: `
      <ng-container *ngFor="let worker of workers">
          <app-worker-details [worker]="worker" [availableProjects]="availableProjects"
                              (dismissWorker)="handleDismissWorker($event)"
                              (assignWorkerToProject)="handleAssignWorkerToProject($event)">
          </app-worker-details>
      </ng-container>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkersViewComponent implements OnInit {

  @Input() workers: Worker[] = [];
  @Input() availableProjects: Project[] = [];
  @Output() dismissWorker: EventEmitter<number> = new EventEmitter<number>();
  @Output() assignWorkerToProject: EventEmitter<ProjectAssignation> = new EventEmitter<ProjectAssignation>();

  constructor() {

  }

  ngOnInit() {
  }

  handleDismissWorker(id: number) {
    this.dismissWorker.emit(id);
  }

  public handleAssignWorkerToProject(projectAssignation: ProjectAssignation): void {
    this.assignWorkerToProject.emit(projectAssignation);
  }
}
