import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-game-speed-panel',
  templateUrl: './game-speed-panel.component.html',
  styleUrls: ['./game-speed-panel.component.scss']
})
export class GameSpeedPanelComponent implements OnInit {

  @Output() gameSpeedChange: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  public gameSpeed(value: number): void {
    this.gameSpeedChange.emit(value);
  }
}
