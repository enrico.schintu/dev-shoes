import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {PlayerConfigurationService} from '../services/player-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardGuard implements CanActivate {
  constructor(private playerConfigurationService: PlayerConfigurationService, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.playerConfigurationService.gameReadyToStart) {
      this.router.navigate(['/login']);
    }
    return !!this.playerConfigurationService.gameReadyToStart;
  }
}

