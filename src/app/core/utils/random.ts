export class Random {
  /**
   *  Generate a random number within the range provided as input.
   * If the params aren't provided then the random number range is between 1 and 999999999
   */
  public static generateRandomNumber(min: number = 1, max: number = 999999999): number {
    return Math.trunc((Math.random() * (max - min)) + min);
  }

  /**
   * Simulate the dice game. The target is the success rate.
   * Example: target = 20 -> 20% of success
   * @param target
   */
  public static dice(target: number): boolean {
    return Random.generateRandomNumber(1, 100) <= target;
  }
}
