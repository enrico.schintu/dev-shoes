/**
 * Utility class for managing math operations
 *
 * @author Add Value S.p.A. by enrico.schintu
 * @version Aug 05, 2019
 *
 */
export class MathUtils {

  public static percentageValue(total: number, value: number): number {
    return Math.round((value * 100) / (total));
  }
}
