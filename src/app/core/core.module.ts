import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TimeHandlerService} from './services/time-handler.service';
import {PlayerConfigurationService} from './services/player-configuration.service';
import {LoginAreaComponent} from './components/login-area/login-area.component';
import {FormsModule} from '@angular/forms';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {WorkersViewComponent} from './components/dashboard/workers-view/workers-view.component';
import {DashboardHeaderComponent} from './components/dashboard/dashboard-header/dashboard-header.component';
import {MarketplaceService} from './services/marketplace.service';
import {ProjectsViewComponent} from './components/dashboard/projects-view/projects-view.component';
import {MarketplaceViewComponent} from './components/dashboard/marketplace-view/marketplace-view.component';
import {TasksViewComponent} from './components/dashboard/projects-view/tasks-view/tasks-view.component';
import { TeamsViewComponent } from './components/dashboard/teams-view/teams-view.component';
import { WorkerDetailsComponent } from './components/dashboard/workers-view/worker-details/worker-details.component';
import { GameSpeedPanelComponent } from './components/dashboard/game-speed-panel/game-speed-panel.component';
import { SavegamePanelComponent } from './components/dashboard/savegame-panel/savegame-panel.component';


@NgModule({
  declarations: [
    LoginAreaComponent,
    DashboardComponent,
    WorkersViewComponent,
    DashboardHeaderComponent,
    ProjectsViewComponent,
    MarketplaceViewComponent,
    TasksViewComponent,
    TeamsViewComponent,
    WorkerDetailsComponent,
    GameSpeedPanelComponent,
    SavegamePanelComponent,
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  providers: [
    TimeHandlerService,
    PlayerConfigurationService,
    MarketplaceService
  ],
  exports: [
    LoginAreaComponent,
    DashboardComponent,
  ]
})
export class CoreModule {
}
