import {Task} from './task';
import {PROJECT_REWARD_RATIO} from '../config/configuration';
import {Random} from '../utils/random';
import {Deserializable} from './deserializable';
import {SkillPoints} from './interfaces';
import {MathUtils} from '../utils/math-utils';

export class Project extends Deserializable<Project> {
  id: number;
  name: string;
  tasks: Task[];
  deadlineCountdown: number = 0;

  constructor(id?: number, name?: string, tasks?: Task[], deadlineCountdown?: number) {
    super();
    this.id = id ? id : Random.generateRandomNumber();
    this.name = name ? name : 'Unknown Project';
    this.tasks = tasks ? tasks : [];
    this.deadlineCountdown = deadlineCountdown ? deadlineCountdown : null;
  }

  /** Remove a worker from this project's tasks */
  public removeWorker(id: number) {
    this.tasks.filter(t => t.workersIds.includes(id)).forEach(t => t.workersIds = t.workersIds.filter(wid => wid !== id));
  }

  /** The total complexity points required to complete the project */
  get totalComplexityPoints(): number {
    return this.tasks.reduce((pts, task) => pts + task.complexityPoints, 0);
  }

  /** The total complexity points required to complete the project */
  get totalCompletedPoints(): number {
    return this.tasks.reduce((pts, task) => pts + task.completedPoints, 0);
  }

  /** The reward obtained once the project is completed */
  get reward(): number {
    return Math.floor(this.totalComplexityPoints * PROJECT_REWARD_RATIO);
  }

  /** True if the Project is completed */
  get completed(): boolean {
    return !this.tasks.find(t => t.complexityPoints > t.completedPoints);
  }

  get progressStatus(): number {
    return MathUtils.percentageValue(this.totalComplexityPoints, this.totalCompletedPoints);
  }

  /** The id of the workers assigned to this project */
  get workersIds(): number[] {
    return this.tasks.reduce((r, t) => [...r, ...t.workersIds], []);
  }

  get skillsRequiredComparation(): SkillPoints[] {
    const skills: SkillPoints[] = this.tasks.reduce((r, t) => {
      const sameSkill = r.find(s => s.skill === t.skillRequired);
      if (!!sameSkill) {
        sameSkill.points = sameSkill.points + t.complexityPoints;
        return r;
      }
      return [...r, {skill: t.skillRequired, points: t.complexityPoints}];
    }, []);
    skills.forEach(s => s.points = MathUtils.percentageValue(this.totalComplexityPoints, s.points));
    return skills;
  }
}

export class ProjectDOM extends Project {
  showDetails: boolean;
  showDevelopers: boolean;
}
