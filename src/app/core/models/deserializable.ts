export class Deserializable<C> {

  public deserialize(input: any): C {
    const self: any = this;
    Object.keys(input).forEach(k => self[k] = input[k]);
    return self;
  }
}
