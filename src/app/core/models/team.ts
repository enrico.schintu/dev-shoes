import {Worker} from './worker';

export class Team {
  id: number;
  name: string;
  workers: Worker[];

  constructor(name: string, ...workers: Worker[]) {
    this.id = Math.random();
    this.name = name;
    this.workers = workers;
  }
}
