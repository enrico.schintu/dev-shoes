import {Worker} from './worker';
import {Team} from './team';
import {BudgetElement} from './interfaces';
import {Project} from './project';
import {COMPANY_INIT_MONEY} from '../config/configuration';
import {Deserializable} from './deserializable';

export class Company extends Deserializable<Company> {
  name: string = 'Unknown Company';
  level: number = 1;
  money: number = COMPANY_INIT_MONEY;
  budget: BudgetElement[];
  teams: Team[] = [];
  projects: Project[] = [];

  constructor(name?: string, ...workers: Worker[]) {
    super();
    this.name = name ? name : 'Unknown Company';
    const starterTeam = new Team('Team 1', ...workers);
    this.teams = [starterTeam];
  }

  public addProject(project): void {
    this.projects.push(project);
  }

  get monltyBudget(): number {
    return this.budget.reduce((result, element) => result + element.value, 0);
  }

  get workers(): Worker[] {
    return this.teams.reduce((r, t) => [...r, ...t.workers], []);
  }

}
