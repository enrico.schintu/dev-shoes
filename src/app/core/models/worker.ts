import {HappinessLevel, RoleType, SkillType, WorkerGender,} from './enums';
import {WORKER_SALARY_RATIO} from '../config/configuration';
import {Random} from '../utils/random';
import {SkillPoints} from './interfaces';
import {Deserializable} from './deserializable';
import {ArrayUtils} from '../utils/array-utils';

export class Worker extends Deserializable<Worker> {
  id: number;
  name: string;
  skills: SkillPoints[];
  role: RoleType;
  currentProjectId: number = null;
  gender: WorkerGender = WorkerGender.MALE;
  age: number = 1;
  energy: number = 100;
  happiness: HappinessLevel = HappinessLevel.HAPPY;
  holidaysFlag: boolean = false;
  isPlayerCharacter: boolean = false;


  constructor(id?: number, name?: string, role?: RoleType, skills?: SkillPoints[], gender?: WorkerGender) {
    super();
    this.id = id ? id : Random.generateRandomNumber();
    this.name = name ? name : 'Unknown Developer';
    this.role = role ? role : RoleType.DEVELOPER;
    this.skills = skills ? skills : [];
    this.gender = gender ? gender : WorkerGender.MALE;
  }

  /** The total productivity of the worker for the skills provided as input */
  public skillProductivity(skills: SkillType[]): number {
    return skills.reduce((points, skill) => points + this.skills.find(s => s.skill === skill).points, 0);
  }

  /** The worker will be busy for holidays (should recharge the energy) */
  public sendInHolidays() {
    this.holidaysFlag = true;
  }

  /** Busy Flag of the worker. */
  get busyFlag(): boolean {
    return !!this.currentProjectId || this.holidaysFlag;
  }

  /** The monthly salary of the worker */
  get salary(): number {
    const skillPts = this.skillProductivity(this.skills.reduce((r, s) => [...r, s.skill], [])) || 0;
    return this.isPlayerCharacter ? 0 : skillPts * WORKER_SALARY_RATIO;
  }

  /** Icon that represents the gender */
  get genderIcon(): string {
    return this.gender === WorkerGender.MALE ? '👦' : '👧';
  }

  get happinessLevel(): HappinessLevel {
    return (this.energy === 0) ? HappinessLevel.ANGRY
      : (this.energy < 15) ? HappinessLevel.TIRED
        : (this.energy < 60) ? HappinessLevel.DISCOMFORT : HappinessLevel.HAPPY;
  }

  get happinessIcon(): string {
    return (this.energy === 0) ? '😠'
      : (this.energy < 15) ? '😵'
        : (this.energy < 60) ? '😐' : '😄';
  }

  get mainlySkill(): SkillType {
    const mainlySkill: SkillPoints = ArrayUtils.orderAscByProperty(this.skills, 'points')[0];
    return mainlySkill.skill;
  }
}
