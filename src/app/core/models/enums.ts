export enum SkillType {
  FRONT_END = 'Front-End',
  BACK_END = 'Back-End',
  MANAGEMENT = 'Management'
}

export enum RoleType {
  DEVELOPER = 'Developer',
  TEAM_LEADER = 'Team Leader',
  PROJECT_MANAGER = 'Project Manager'
}

export enum TimeTriggerType {
  YEARLY_TRIGGER = 'YEARLY_TRIGGER',
  MONTHLY_TRIGGER = 'MONTHLY_TRIGGER',
  WEEKLY_TRIGGER = 'WEEKLY_TRIGGER'
}

export enum WorkerGender {
  MALE = 'Male',
  FEMALE = 'Female'
}

export enum HappinessLevel {
  ANGRY = 'Angry',
  TIRED = 'Tired',
  DISCOMFORT = 'Discomfort',
  HAPPY = 'Happy'
}
