import {SkillType, TimeTriggerType, WorkerGender} from './enums';
import {Company} from './company';

export interface BudgetElement {
  id: number;
  name: string;
  value: number;
}

export interface TriggerAction {
  triggerType: TimeTriggerType;
  action: Function;
}

export interface Player {
  id: number;
  username: string;
  password?: string;
}

export interface SaveGameStatus {
  player: Player;
  company: Company;
  time: number;
}

export interface SkillPoints {
  skill: SkillType;
  points: number;
  label?: string;
}

export interface StarterInfo {
  username: string;
  companyName: string;
  gender: WorkerGender;
}

export interface HeaderInfo {
  currentMoney: number;
  currentTime: string;
}

export interface ProjectAssignation {
  projectId: number;
  workerId: number;
}
