import {SkillType} from './enums';
import {Deserializable} from './deserializable';

export class Task extends Deserializable<Task> {
  id: number;
  description: string;
  skillRequired: SkillType;
  complexityPoints: number;
  completedPoints: number = 0;
  workersIds: number[] = [];

  constructor(id?: number, description?: string, complexityPoints?: number, skillType?: SkillType) {
    super();
    this.id = id;
    this.description = description;
    this.complexityPoints = complexityPoints;
    this.skillRequired = skillType;
  }
}
