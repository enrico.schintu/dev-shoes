import {Injectable} from '@angular/core';
import {TimeHandlerService} from './time-handler.service';
import {SkillPoints, TriggerAction} from '../models/interfaces';
import {RoleType, SkillType, TimeTriggerType} from '../models/enums';
import {Project} from '../models/project';
import {Worker} from '../models/worker';
import {MARKET_PROJECTS_PER_TICK, MARKET_WORKERS_PER_TICK} from '../config/configuration';
import {Task} from '../models/task';
import {Random} from '../utils/random';
import {PlayerConfigurationService} from './player-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class MarketplaceService {

  private currentProjects: Project[];
  private currentWorkers: Worker[];

  private workersCreatedCount: number = 0;
  private projectsCreatedCount: number = 0;

  constructor(private timeHandlerService: TimeHandlerService, private playerConfigurationService: PlayerConfigurationService) {
    this.generateWorkers();
    this.generateProjects();
    this.initTimeEvents();
  }

  public acquireProject(id: number): void {
    const target = this.currentProjects.find(p => p.id === id);
    if (!!target) {
      this.playerConfigurationService.company.addProject(target);
      this.currentProjects = this.currentProjects.filter(p => p.id !== id);
    }
  }

  public acquireWorker(id: number): void {
    const target = this.currentWorkers.find(w => w.id === id);
    if (!!target) {
      this.playerConfigurationService.company.teams[0].workers.push(target);
      this.currentWorkers = this.currentWorkers.filter(w => w.id !== id);
    }
  }

  private initTimeEvents(): void {
    const createProjects: TriggerAction = {
      triggerType: TimeTriggerType.MONTHLY_TRIGGER,
      action: () => this.generateProjects()
    };

    const createWorkers: TriggerAction = {
      triggerType: TimeTriggerType.MONTHLY_TRIGGER,
      action: () => this.generateWorkers()
    };

    this.timeHandlerService.addTriggerActions(createProjects, createWorkers);
  }

  private generateProjects(): void {
    this.currentProjects = [];
    for (let i = 0; i < MARKET_PROJECTS_PER_TICK; i++) {
      this.currentProjects.push(this.generateProject());
    }
  }

  private generateWorkers(): void {
    this.currentWorkers = [];
    for (let i = 0; i < MARKET_WORKERS_PER_TICK; i++) {
      this.currentWorkers.push(this.generateWorker());
    }
  }

  private generateWorker(): Worker {
    this.workersCreatedCount++;
    const companyLevel: number = this.playerConfigurationService.company.level;
    const fe: SkillPoints = {skill: SkillType.FRONT_END, points: Random.generateRandomNumber(1, 200 * companyLevel), label: 'FE'};
    const be: SkillPoints = {skill: SkillType.BACK_END, points: Random.generateRandomNumber(1, 200 * companyLevel), label: 'BE'};
    return new Worker(new Date().getTime() + this.workersCreatedCount, `Worker_${this.workersCreatedCount}`, RoleType.DEVELOPER, [fe, be]);
  }

  private generateProject(): Project {
    this.projectsCreatedCount++;
    const companyLevel: number = this.playerConfigurationService.company.level;
    const fe1: Task = new Task(Random.generateRandomNumber(), 'Analyze Web Flow', Random.generateRandomNumber(1, 500 * companyLevel), SkillType.FRONT_END);
    const be1: Task = new Task(Random.generateRandomNumber(), 'Analyze Services', Random.generateRandomNumber(1, 500 * companyLevel), SkillType.BACK_END);
    const fe2: Task = new Task(Random.generateRandomNumber(), 'Create Web Components', Random.generateRandomNumber(1, 500 * companyLevel), SkillType.FRONT_END);
    const be2: Task = new Task(Random.generateRandomNumber(), 'Create Services', Random.generateRandomNumber(1, 500 * companyLevel), SkillType.BACK_END);
    const complexityPts: number = [fe1, be1, fe2, be2].reduce((r, c) => r = r + c.complexityPoints, 0);
    return new Project(new Date().getTime() + this.projectsCreatedCount, `Project_${this.projectsCreatedCount}`, [fe1, be1, fe2, be2], this.calculateProjectDeadLine(complexityPts));
  }

  private calculateProjectDeadLine(projectComplexityPts: number): number {
    return Math.trunc(projectComplexityPts * 0.02);
  }

  get availableWorkers(): Worker[] {
    return this.currentWorkers;
  }

  get availableProjects(): Project[] {
    return this.currentProjects;
  }
}
