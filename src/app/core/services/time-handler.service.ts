import {Injectable} from '@angular/core';
import {GAME_TICK_DELAY, GAME_WEEKS_IN_MONTH} from '../config/configuration';
import {TriggerAction} from '../models/interfaces';
import {TimeTriggerType} from '../models/enums';

@Injectable({
  providedIn: 'root'
})
export class TimeHandlerService {

  public currentGameTime: number = 0;

  private timeInterval: any = null;
  private gameSpeed: number = 1;
  private triggerActions: TriggerAction[] = [];

  constructor() {
  }

  /** Start the game */
  public startGame(): void {
    this.timeInterval = setInterval(() => this.handleTimeTick(), GAME_TICK_DELAY / this.gameSpeed);
  }

  /** Stop the game */
  public stopGame(): void {
    clearInterval(this.timeInterval);
  }

  /** Add new Time Triggered Actions */
  public addTriggerActions(...actions: TriggerAction[]): void {
    this.triggerActions.push(...actions);
  }

  public changeGameSpeed(value: number) {
    this.stopGame();
    if (!!value) {
      this.gameSpeed = value;
      this.startGame();
    }
  }

  /** Handle all the actions that should be triggered at specific time */
  private handleTimeTick(): void {
    const prevYear = this.year;
    const prevMonth = this.month;
    const prevWeek = this.week;
    this.currentGameTime++;
    if (prevYear !== this.year) {
      this.triggerActions.filter(a => a.triggerType === TimeTriggerType.YEARLY_TRIGGER).forEach(a => a.action.apply(this, [this.year]));
    }
    if (prevMonth !== this.month) {
      this.triggerActions.filter(a => a.triggerType === TimeTriggerType.MONTHLY_TRIGGER).forEach(a => a.action.apply(this, [this.month]));
    }
    if (prevWeek !== this.week) {
      this.triggerActions.filter(a => a.triggerType === TimeTriggerType.WEEKLY_TRIGGER).forEach(a => a.action.apply(this, [this.week]));
    }
  }

  get week(): number {
    return Math.trunc(this.currentGameTime % GAME_WEEKS_IN_MONTH + 1);
  }

  get month(): number {
    return Math.trunc((this.currentGameTime / GAME_WEEKS_IN_MONTH) % 12) + 1;
  }

  get year(): number {
    return Math.trunc((this.currentGameTime / (GAME_WEEKS_IN_MONTH * 12)) + 1);
  }

  get currentTime(): string {
    return `Y: ${this.year} | M: ${this.month} | W: ${this.week}`;
  }
}
