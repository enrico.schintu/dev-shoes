import {Injectable} from '@angular/core';
import {Company} from '../models/company';
import {SAVE_GAME_KEY, WORKER_ENERGY_DROP_TICK, WORKER_ENERGY_FILL_TICK, WORKER_LEAVE_PERCENTAGE} from '../config/configuration';
import {Player, ProjectAssignation, SaveGameStatus, StarterInfo, TriggerAction} from '../models/interfaces';
import {Worker} from '../models/worker';
import {Random} from '../utils/random';
import {RoleType, SkillType, TimeTriggerType} from '../models/enums';
import {TimeHandlerService} from './time-handler.service';
import {NotificationsService} from '../../shared/notifications/services/notifications.service';
import {MessageType} from '../../shared/models/enums';
import {Router} from '@angular/router';
import {Project} from '../models/project';

@Injectable({
  providedIn: 'root'
})
export class PlayerConfigurationService {

  private player: Player = null;
  private userCompany: Company = null;

  constructor(private router: Router,
              private timeHandlerService: TimeHandlerService,
              private notificationsService: NotificationsService) {

    this.initTimeEvents();
  }

  public startGame(): void {
    this.loadSaveGame();
    if (!this.userCompany) {
      this.initGame();
    } else {
      this.timeHandlerService.startGame();
    }
  }

  public resetGame(info: StarterInfo): void {
    this.timeHandlerService.stopGame();
    this.player = {id: Random.generateRandomNumber(), username: info.username};
    this.initUserCompany(info);
    this.timeHandlerService.currentGameTime = 0;
    this.timeHandlerService.startGame();
    this.router.navigate(['/dashboard']).then(() => this.saveGame());
  }

  public initGame(): void {
    this.timeHandlerService.stopGame();
    this.player = null;
    this.userCompany = null;
    this.timeHandlerService.currentGameTime = 0;
    this.router.navigate(['/login']).then(() => this.removeSaveGame());
  }

  /** Reload a previous game state */
  public loadSaveGame(): void {
    const saveGameStatus: SaveGameStatus = JSON.parse(window.localStorage.getItem(SAVE_GAME_KEY));
    if (saveGameStatus) {
      const savedCompany = new Company().deserialize(saveGameStatus.company);
      savedCompany.teams.forEach(t => t.workers = t.workers.map(i => new Worker().deserialize(i)));
      savedCompany.projects = savedCompany.projects.map(p => new Project().deserialize(p));
      this.userCompany = savedCompany;
      this.player = saveGameStatus.player;
      this.timeHandlerService.currentGameTime = saveGameStatus.time;
    }
  }

  public removeSaveGame(): void {
    window.localStorage.removeItem(SAVE_GAME_KEY);
  }

  /** Save the current game state */
  public saveGame(): void {
    const saveGameStatus: SaveGameStatus = {time: this.timeHandlerService.currentGameTime, company: this.company, player: this.player};
    window.localStorage.setItem(SAVE_GAME_KEY, JSON.stringify(saveGameStatus));
  }

  /** Create the player company */
  private initUserCompany(info: StarterInfo): void {
    const firstWorker = new Worker(null, this.player.username, RoleType.DEVELOPER,
      [{skill: SkillType.FRONT_END, points: 100, label: 'FE'}, {skill: SkillType.BACK_END, points: 100, label: 'BE'}],
      info.gender);
    firstWorker.isPlayerCharacter = true;
    this.userCompany = new Company(info.companyName, firstWorker);
  }

  private initTimeEvents(): void {
    const automaticSaveGame: TriggerAction = {
      triggerType: TimeTriggerType.YEARLY_TRIGGER,
      action: () => {
        this.notificationsService.showNotifications({message: `💾 Game saved!`, level: MessageType.INFO});
        this.saveGame();
      }
    };

    const monthlySalary: TriggerAction = {
      triggerType: TimeTriggerType.MONTHLY_TRIGGER,
      action: () => {
        if (!!this.company.workers.length) {
          this.notificationsService.showNotifications({
            message: `💸 Employee salary payment: -${this.salaries} €`,
            level: MessageType.INFO
          });
          this.userCompany.money = this.companyMoney - this.salaries;
        }
      }
    };

    const projectStatusUpdate: TriggerAction = {
      triggerType: TimeTriggerType.WEEKLY_TRIGGER,
      action: () => {
        this.company.workers.filter(w => !!w.currentProjectId && !w.holidaysFlag)
          .forEach(worker => {
            const currentProject = this.company.projects.find(p => p.id === worker.currentProjectId);
            if (!currentProject) {
              // The project doesn't exists anymore...
              worker.currentProjectId = null;
            } else {
              // Update Project tasks
              const taskToUpdate = currentProject.tasks.find(t => t.workersIds.includes(worker.id));
              const workerSkillPoints = worker.skillProductivity([taskToUpdate.skillRequired]);
              const unspentPoints = taskToUpdate.completedPoints + workerSkillPoints - taskToUpdate.complexityPoints;
              if (unspentPoints >= 0) {
                // Find next task that should be completed in this project
                const nextTask = currentProject.tasks.find(nt => nt.id !== taskToUpdate.id && nt.completedPoints < nt.complexityPoints);
                if (nextTask) {
                  nextTask.workersIds = taskToUpdate.workersIds.map(id => id);
                  nextTask.completedPoints = unspentPoints;
                } else {
                  currentProject.workersIds.forEach(workerId => {
                    const workerOfTheProject: Worker = this.company.workers.find(wk => wk.id === workerId);
                    !!workerOfTheProject ? workerOfTheProject.currentProjectId = null : currentProject.removeWorker(workerId);
                  });
                }
                taskToUpdate.workersIds = [];
              }
              taskToUpdate.completedPoints = Math.min(taskToUpdate.complexityPoints, taskToUpdate.completedPoints + workerSkillPoints);

              if (currentProject.completed) {
                // Remove al Workers references
                this.company.workers.filter(w => w.currentProjectId === currentProject.id).forEach(w => w.currentProjectId = null);
                this.company.money = this.company.money + currentProject.reward;
                this.notificationsService.showNotifications({
                  message: `💰 Project completed: +${currentProject.reward} €`,
                  level: MessageType.SUCCESS
                });
                this.company.projects = this.company.projects.filter(pj => currentProject.id !== pj.id);
              }
            }
          });
        // Update the projects deadline countdown
        this.company.projects.forEach(p => p.deadlineCountdown--);
        this.company.projects.filter(p => !p.deadlineCountdown).forEach(p => {
          this.notificationsService.showNotifications({
            message: `💩 Unsuccessful Project: -${p.reward} €`,
            level: MessageType.DANGER
          });
          this.company.money = this.company.money - p.reward;
          this.company.projects = this.company.projects.filter(pj => p.id !== pj.id);
        });
      }
    };

    const workersEnergyUpdate: TriggerAction = {
      triggerType: TimeTriggerType.WEEKLY_TRIGGER,
      action: () => {
        if (!!this.company.workers.length) {
          this.company.workers.filter(w => !w.holidaysFlag && !!w.currentProjectId).forEach(w => {
            w.energy = Math.max(0, w.energy - WORKER_ENERGY_DROP_TICK);
            if (!w.energy && Random.dice(WORKER_LEAVE_PERCENTAGE)) {
              this.notificationsService.showNotifications({
                message: `👋 ${w.name} is no longer part of the Company`,
                level: MessageType.DANGER
              });
              this.dismissWorker(w.id);
            }
          });
          this.company.workers.filter(w => w.holidaysFlag).forEach(w => {
            w.energy = Math.min(100, Math.min(100, w.energy + WORKER_ENERGY_FILL_TICK));
            if (w.energy === 100) {
              w.holidaysFlag = false;
            }
          });
        }
      }
    };

    this.timeHandlerService.addTriggerActions(automaticSaveGame, monthlySalary, workersEnergyUpdate, projectStatusUpdate);
  }

  public dismissWorker(workerId: number): void {
    this.company.teams.forEach(t => t.workers = t.workers.filter(w => w.id !== workerId));
  }

  public assignWorkerToProject(request: ProjectAssignation): void {
    const project = this.company.projects.find(p => p.id === request.projectId);
    const worker = this.company.workers.find(w => w.id === request.workerId);

    if (!!project && !!worker) {
      if (!!worker.currentProjectId) {
        const oldProject = this.company.projects.find(p => p.id === worker.currentProjectId);
        if (!!oldProject) {
          oldProject.tasks.forEach(t => t.workersIds = t.workersIds.filter(id => id !== worker.id));
        }
      }
      worker.currentProjectId = project.id;
      const nextTask = project.tasks.find(t => t.completedPoints < t.complexityPoints);
      if (!!nextTask) {
        nextTask.workersIds.push(worker.id);
      }
    }

  }

  get company(): Company {
    return this.userCompany;
  }

  get gameReadyToStart(): boolean {
    return !!this.player && !!this.userCompany;
  }

  get companyMoney(): number {
    return this.userCompany.money || 0;
  }

  get salaries(): number {
    return this.company.teams.reduce((r, t) => {
      return r + t.workers.reduce((s, w) => s = s + w.salary, 0);
    }, 0);
  }

  get activeProjects(): Project[] {
    return this.company.projects.filter(p => !p.completed);
  }

}
